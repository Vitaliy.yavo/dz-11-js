document.addEventListener("DOMContentLoaded", function () {
  const passwordInputs = document.querySelectorAll(
    ".input-wrapper input[type='password']"
  );
  const eyeIcons = document.querySelectorAll(".input-wrapper i.icon-password");

  eyeIcons.forEach((icon, index) => {
    icon.addEventListener("click", function () {
      const isPasswordVisible = passwordInputs[index].type === "text";
      passwordInputs[index].type = isPasswordVisible ? "password" : "text";
      icon.classList.toggle("fa-eye");
      icon.classList.toggle("fa-eye-slash");
    });
  });

  const submitButton = document.querySelector(".btn");
  const confirmPasswordInput = passwordInputs[1];

  submitButton.addEventListener("click", function (event) {
    event.preventDefault();

    const firstPassword = passwordInputs[0].value;
    const secondPassword = confirmPasswordInput.value;

    if (firstPassword === secondPassword) {
      alert("You are welcome");
    } else {
      const errorMessage = document.createElement("p");
      errorMessage.textContent = "Потрібно ввести однакові значення";
      errorMessage.style.color = "red";
      confirmPasswordInput.insertAdjacentElement("afterend", errorMessage);
    }
  });
});
